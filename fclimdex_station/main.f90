! ------------------------------------------------------------------------
! FClimDex project
!
! Fortran implementation of RClimDex (which in turn is an implementation
! in R of the Microsoft Excel ClimDex program) for calculating
! indices of climate extremes.
!
! FClimDex and RClimDex were originally written and maintained at the
! Climate Research Branch of Meteorological Service of Canada.
!
! Based on the version provided by Lisa Alexander @ 2011.4.11
! To simplify, modify and improve the code by H.Yang from 2011.4.12
!
! This version is for station data only -- no netcdf format.
!
! How-to-Run and Change History can be found in the "readme.txt"
!
! last modified @ 2012-07-19
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
! **********     Main program starts from here	 ********************* !
!
   program Fclimdex
   use COMM
   implicit none
   integer :: OMP_GET_NUM_THREADS,OMP_GET_THREAD_NUM,IDcpu, &  ! used only for OpenMP
              ID,status, &         ! unit ID for I/O, and error status (0 means OK)
              i
   integer(4)       :: stnnum      ! # of stations
   double precision :: tfrom,tto   ! current time in seconds
   namelist /input/ ipt_dir,opt_dir,para_file,data_dir,inf_file,log_file,OS,save_thresholds,NoMissingThreshold,cal_EHI

! This is used for OpenMP run, to print some information on screen.
!$OMP parallel private(IDcpu);  IDcpu=omp_get_thread_num();   print*,'OpenMP ID #',IDcpu,'of ',omp_get_num_threads()
!$OMP end parallel

 ! default values for the run, can be re-set in the "input.nml" file.
    save_thresholds=.false.  ! whether to save the thresholds
    NoMissingThreshold=0.85  ! default value, suggested by Lisa
    cal_EHI=.false.          ! whether to calculate Heat Wave Indices

    SS=int(WINSIZE/2)

    call getID(ID)        ! open namelist file
    open (ID, file="input.nml",status='OLD')
    read(ID,nml=input)
    close(ID)
    if(OS/='windows'.and.OS/='linux') stop 'Error in input.nml: OS wrong !'

    call getID(ID_log)    ! log file
    open (ID_log, file=trim(opt_dir)//trim(log_file),IOSTAT=status)
    if(status.ne.0) then
      close(ID_log)
      stop 'ERROR in inp.nml: the opt_dir does NOT exist, or permission wrong !'
    endif

    call check_folders  ! check if folder exists, if not, create it.
     ! whether to open a single index file for all stations

    stnnum=1

    call get_time(0,ID_log,tfrom)   ! get initial time for this running

! start loop for each station: read infile, params, calculate indices...
    do
      call read_file(stnnum)
      if(iend==1) exit   ! finish all stations, running is over...

      do i=1,N_index
        ofile(i)=trim(opt_dir)//trim(folder(i))//sub//trim(Oname)//trim(folder(i))//'.txt'
      enddo
      if(stnnum==1) then
        print*,'The output names look like this:'
        print*,trim(ofile(1))
        print*,'If you"re not satisfied, please change line 69 in the "main.f90" ...'
        print*,''
      endif

      BYRS=BASEEYEAR-BASESYEAR+1   ! Base year #

      call qc   ! Quality control of data, and check if all data are missing
      if(Tmax_miss .and. Tmin_miss .and. Prcp_miss) then
        print*,'WARNING: all data are missing, this station is ignored ...'
        write(ID_log,'(a20,a)') stnID,': all data are MISSING ....'
        stnnum=stnnum+1
        cycle
      endif

      call FD     ! FD, SU, ID, TR
      call GSL    ! GSL
      call TXX    ! TXx, TXn, TNx, TNn, DTR, ETR
      call Rnnmm  ! R10mm, R20mm, Rnnmm, SDII
      call RX5day ! Rx1day, Rx5day
      call CDD    ! CDD, CWD
      call R95p   ! R95p, R99p, PRCPTOT, R95pTOT, R99pTOT
      call TX10p  ! TX10p, TN10p, TX50p, TN50p, TX90p, TN90p, save_thresholds, WSDI, CSDI

      if(cal_EHI) call HeatWave ! EHIa, EHIs, EHF

      stnnum=stnnum+1

    enddo   ! end loop over all stations

    stnnum=stnnum-1
    write(ID_log,*) "Total ",stnnum," stations were calculated !"

    call get_time(1,ID_log,tto)  ! get final running time
    close(ID_log)

    print*,'Total time usage is about ',real(tto-tfrom),' seconds..'

    stop
    end program Fclimdex
