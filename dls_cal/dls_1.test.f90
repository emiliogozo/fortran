!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                                     !
! MODULE        :  COMM                                               !
!                                                                     !
! PROGRAM       :  ~ClimdEX/Source/F90/dls_cal/dls_1.1.f90            !
!                                                                     !
! PURPOSE       :  TO CALCULATE MONTHLY/SEASONAL/ANNUAL DECORRELATION !
!                  LENGTH SCALES (DLS) FOR EACH ETCCDI INDEX IN ORDER !
!                  TO CREATE GRIDDED OUTPUT FOR GHCNDEX/HADEX USING   !
!                  ANGULAR DISTANCE WEIGHTING.                        !
!                                                                     !
!                                                                     !
! DESCRIPTION   :  STATION PAIRS ARE CORRELATED WITHIN EACH LATITUDE  !
!                  BAND AS A FUNCTION OF DISTANCE                     !
!                  CORRELATIONS ARE PUT INTO 100KM BINS AND A         !
!	           POLYNOMIAL FUNCTION IS FITTED TO THE BINNED DATA   !
!                  E-FOLDING DISTANCE IS CALCULATED FROM THE FITTED   !
!                  FUNCTION AND BECOMES THE 'SEARCH RADIUS'           !
!                  DLS IS INTERPLOATED ACROSS EACH LATITUDE GRIDBOX   !
!                  USING THE CENTRE OF THE LAT BAND AS THE DLS VALUE  !
!                  FOLLOWS THE METHOD OF ALEXANDER ET AL. 2006        !
!                                                                     !
!                                                                     !
! CALLED BY     :                                                     !
!                                                                     !
!                                                                     !
! CALLS         :                                                     !
!                                                                     !
!                                                                     !
! I/O           :  INPUT STATION FILE                                 !
!   e.g. ../HQ_Aus_stats/....                                         !
!                                                                     !
!                                                                     !
!                                                                     !
! AUTHOR        : Lisa Alexander  (Original code in IDL)              !
!                 HONGANG YANG    (Fortran code)                      !
!
!                                                                     !
! CHANGE RECORD :   updated 12/12/2011 LVA                            !
!                   bug fixed 8/3/2012 HY                             !
!                   updated 20/6/2012 HY                              !
!                 add user-defined lat bands                          !
!                                                                     !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


module COMM

 implicit none
 real,parameter           :: mdi=-99.9                 !missing data indicator
 integer                   :: distance                  !number of bins in units of 100KM. Station distance must be less than this value to be useful.
 integer                   :: maxny                     !max number of allowable years
 integer                   :: monthf                    !starting month. If ONLY annual value, monthf=13
 integer                   :: N_month                   !number of monthly/annual index, can be 13 (12 monthly and 1 annual) or 1 (only annual)
 integer                   :: Nlat,Nlon                 !number of lats and lons
 real                       :: dls_default               !minimum dls default value
 character*250             :: outfile, &                !dls output file
                              grid_file                 !contains grid locations (lat,lon)
 character*3               :: monthnames(0:12)          !month names as 3 characters
 real,allocatable,save     :: lat(:),lon(:)             !contains all the lat and lon from grid_file
 integer, allocatable,save :: dlss(:,:,:)               !dls values for output (after interpolation)
 data  monthnames/'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec','Ann'/

end module COMM

! -----------------------------------------------------------------------------------------
! END MODULE COMM - START MAIN PROGRAM
! -----------------------------------------------------------------------------------------

!
!  This code is specially designed for Imke to check why it takes too long for a run.
!
!

program distcorrfun_angles_wfun  ! IDL code from Lisa @ 2011.7.18
use COMM
 
 implicit none
 character*200             :: infile                     !input file
 character*200             :: statfile                   !file containing station list
 character*200             :: inp_dir                    !location input directory
 character*200             :: opt_dir                    !location output directory
 character*50              :: ind                        !indicator name e.g. 'TXx'
 character*50              :: statnums                   !station numbers/names (required to read in correct data)
 character*50              :: junk                       !unnecessary line in input file
 character*50              :: goodfit                    !determine whether function is a good fit
 logical                   :: ann_only                   !determine whether input data contain only annual data
 logical                   :: exists                     !determine whether file exists
 integer                   :: reqmon                     !required month/annual
 integer                   :: status                     !file I/O status
 integer                   :: ID1,ID3,ID4                !file numbers
 integer                   :: i,j,k,n,ii,i0,j0           !loop integer counters
 integer                   :: min_year                   !required miniumum year for analysis
 integer                   :: n_yridx,n_yridx0           !data counters
 integer                   :: N_yridx1,N_yridx2          ! "     "
 integer                   :: ct,ct1,cnt                 !loop counters
 integer                   :: N_newbinarray              !array for bin counting
 integer                   :: nstats                     !number of stations in the stat file.
 integer                   :: N_sta                      !number of stations in a latitude band.
 integer                   :: N_mdiidx1,N_mdiidx2        !missing data indices
 integer                   :: nc                         !exponential function counter (to help find dls)
 integer                   :: binnum                     !number of bins required
 integer                   :: Syear                      !start year for dls calculation
 integer                   :: Syr                        ! start year for the data
 integer                   :: Ndegree                    !degree of polynomial 
 integer                   :: N_lat                      !number of latitude bands
 integer                   :: tarr(8)                    ! temporary array to save date and time from system
 real                      :: lat_width(200)             !lat band width, read from namelist
 real                      :: llat,llon,aalt             !latitude, longitude, altitude
 real                      :: nearest_cdd                !dls val
 real                      :: tmp                        !temporary variable
 real                      :: thetak,thetai              !bearings (in radians) for ADW calculation
 real                      :: parametersk                !parameter for ADW calculation
 real                      :: parametersi                !   "       "   "      "
 real                      :: avgarr1,avgarr2            !array averages
 real                      :: rcorr,correlate            !correlation statistics
 real                      :: var,zvalue,pvalue          !output statistics
 real                      :: gauss_pdf                  !Gaussian probability function
 real                      :: epi                        !exponential function
 real                      :: latlondist                 !distance between stations
 real                      :: chisq                      !chi squared statistic
 real                      :: error                      !tolerance for good fit
 real                      :: latF,latT,r1,r2            !temporary variable
 real                      :: data(0:13),data2(0:1)      !temporary arrays
 real,allocatable          :: newdata(:,:,:), &          !temporary array to hold all data for a station
                              polyres(:), &              !fitting coefficients
                              yfit(:), &                 !Vector of (polynomial fit) calculated Y's in poly_fit (least squares)
                              sigma(:), &                !The 1-sigma error estimates of the returned parameters
                              latc(:), &                 !center of the latitude bands (degrees)
                              bin_avgs(:,:), &           !array to hold correlation coefficients
                              xbin(:), &                 !array for station distance (km)
                              newavg(:), &               !the mean of a given array
                              sdevx(:), &                !the Standard deviation of a given array
                              xn(:), &                   !array holding index 1--distance*100 for fitting curve
                              xfn(:)                     !fitting curve, function of xn
 integer,allocatable       :: mdiidx1(:),  &             !index array for valid data
                              mdiidx2(:), &              ! "           "            " 
                              yridx(:), &                !temporary array used in SetIntersection
                              dls(:,:), &                !dls values at each latitude band center (latc)
                              Ista(:), &                 !station index array within a latitude band.
                              ctb(:)                     !bin counter
 real,allocatable          :: arr1(:),arr2(:),  &        !temporary array holding index values
                              newbinarray(:), &          !array to hold valid correlation coefficients
                              lata(:),lona(:), &         !temporary array to hold all lat/lon from grid file.
                              tempa(:),tmpa(:), &        !temporary array to hold lat/lon values
                              tmpb(:),  &                !"       "        "
                              interp(:)                  !temporary array to hold interpolated dls values
 character*50,allocatable  :: statnum(:), &              !station name
                              goodfits(:,:)              !whether the fitting is ok ('yes' or 'no')
 double precision          :: time00

 namelist /paras/Ndegree,ind,Syear,infile,statfile,opt_dir,inp_dir,grid_file,lat_width,error,min_year,dls_default,distance,maxny

!!!!!!!!!!!!!!!!!!!!!!set up defaults for input variables!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 print*,'Now code starts:'
 call get_time(time00)

  monthf=1  ! starting month, if ONLY annual value, monf=13
  ann_only=.false.
  nearest_cdd=dls_default
  lat_width=-99.9   ! initialize the lat_band_width
  
!!!!!!!!!!!!!!!!!!!!Read in control parameters from namelist!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  call getID(ID1)
  open(ID1,file='dls_1.1.nml',status='OLD',IOstat=status)
  if(status/=0) stop 'Error: can NOT find the input file "dls_1.1.nml" ..'
  read(ID1,nml=paras)
  close(ID1)
  print*,''
  print('(a,i5,a)'),'NOTE: the station distance is ',distance*100,'km, but you can change it if necessary!'

!!!!!!!!!!!!!!!!!!!get output file name, and we will add '.txt' and/or '.nc' to this name later...!!!!!!!!!!!!!!!!!!!!

  outfile=trim(opt_dir)//'_'//trim(ind)//'_dls'

!!!!!!!!!!!!!!!!!!check input parameters ...!!!!!!!!!!!!!!!!!!!!!!!!!!

  if(Syear <1000)   stop 'Please change the Syear value and try again ...'       !
  if(Ndegree<1)     stop 'Please change the Ndegree value and try again ...'
  if(min_year <2)   stop 'Please change the min_year value and try again ...'
  if(error <0.0001) stop 'Please change the error value and try again ...'
  if(distance<2)    stop 'Please change the distance and try again ...'
  if(maxny<2)       stop 'Please change the maxny and try again ...'

!!!!!!!!!! get lat band width values and center position latc  !!!!!!!!!!!!!!!

  N_lat=count(lat_width > 0.)
  if(N_lat>1) then               ! non-uniform lat band width
    allocate(latc(N_lat))
    latc=-99.9
    if(abs(sum(lat_width(1:N_lat))-180.)>1.e-4) then
       print*,'You"re using user-defined lat_width values,'
       print*,'So the total of them should equal to 180.!'
       stop 'Please change the lat_width array in nml and try again...'
    endif
    do i=1,N_lat
      latc(i)=-90.+sum(lat_width(1:i))-lat_width(i)/2.   ! dls is on the lat band center -- latc
    enddo

  else                         ! uniform lat band width
    if(lat_width(1)>180.) lat_width(1)=180.
    N_lat=int(180./lat_width(1))
    if(abs(N_lat*lat_width(1)-180.) > 1.e-4) then
       print*,'You"re using single lat_width value,'
       print*,'So we have uniform lat band width in the run,'
       stop 'Please change the lat_width in nml and try again...'
    endif
    allocate(latc(N_lat))
    lat_width(1:N_lat)=lat_width(1)
    do i=1,N_lat
      latc(i)=-90.+(i-0.5)*lat_width(1)   ! dls is on the lat band center -- latc
    enddo
  endif

!!!!!!!!!!!!!!!!!!!check if the station-list and grid files exist!!!!!!!!!!!!!!!!!!!

  inquire(file=statfile,exist=exists)
  if (.not.exists)then
    print*,'WARNING: this "statfile" does NOT exist:'
    print*,trim(statfile)
    stop 'Now code stops !'
  endif

  inquire(file=grid_file,exist=exists)
  if (.not.exists)then
    print*,'WARNING: this "grid_file" does NOT exist:'
    print*,trim(grid_file)
    stop 'Now code stops !'
  endif

   ! to get first year - Syr
  call Date_and_Time(values=tarr)
  Syr=tarr(1)-maxny+1

  allocate(bin_avgs(0:distance,0:999999),xbin(0:distance),newavg(0:distance),sdevx(0:distance), &
    xn(0:(distance*100-1)),xfn(0:(distance*100-1)),ctb(0:distance))

!!!!!!!!!!!!!!!!!!Read in station locations (lat, lon) from statfile!!!!!!!!!!!!!!!!
  call getID(ID3)
  open(ID3,file=statfile)
  ct1=0
  statnums=''
  i=0 ! get station #
  
  do
    read(ID3,*,end=101) statnums
    i=i+1
  enddo
101 nstats=i-1
  rewind(ID3)

  allocate(statnum(0:nstats),newdata(0:nstats,0:13,0:maxny-1),polyres(0:Ndegree),yfit(0:ndegree-1), &
        sigma(0:ndegree),lat(0:nstats),lon(0:nstats))
  data=mdi
  data2=mdi
  newdata=mdi  ! initialize data with missing data

  do i=0,nstats
    read(ID3,*) statnums,llat,llon  ! In this way, We don't care about the format of the original data.
    lat(i)=llat
    lon(i)=llon
    statnum(i)=statnums
  infile=trim(inp_dir)//trim(statnum(i))//'_rclimdex_daily.txt_'//trim(ind)  !!REMOVE UNNECESSARY INFILES WHEN SURE NOT REQUIRED
!    infile=trim(inp_dir)//trim(statnum(i))//'_'//trim(ind)//'.txt'  ! index file
!  infile='./HQ_Aus_stats/'//trim(statnum(i))//'_rclimdex_daily.txt'//'_'//trim(ind)
!  infile=trim(inp_dir)//trim(statnum(i))//trim(ind)

    inquire(file=infile,exist=exists)
    if (.not.exists)then
      print*,'WARNING: this infile does NOT exist:'
      print*,trim(infile)
      stop 'So code stops'
    else
      call getID(ID1)    ! read in index
      open(ID1,file=infile)
      read(ID1,'(A50)') junk
      if((i==0) .and. (len_trim(junk)<20)) ann_only=.true.
      if(ann_only) then  ! Only annual index in the files
        monthf=13
        do
          read(ID1,*,end=100) data2
          ct=data2(0)-Syr
          if(ct <1) stop '"maxny" too small, Please change it and try again...'
          newdata(i,0,ct)=data2(0)
          newdata(i,1:13,ct)=data2(1)
        enddo
      else         ! 12 monthly and 1 annual indices in the files
        do
          read(ID1,*,end=100) data
          ct=data(0)-Syr
          if(ct <1) stop '"maxny" too small, Please change it and try again...'
          newdata(i,0:13,ct)=data
        enddo
      endif
100 close(ID1)
    endif
  enddo  ! i loop for stations
  
  close(ID3)
  print('(a,i5,a)'),'finish reading ',nstats+1,' station data ..'
  if(ann_only) print*,'Note: only annual values for this index ...'
!!!!!!!!!!!!!!!!!!!!!!!!Finished reading all station locations!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!Set up variables for dls calculation!!!!!!!!!!!!!!!!!!!

  xbin=(/ (i, i = 0, distance) /)*100.
  xn=(/(i,i=0,(distance*100-1))/)*1.0
  N_month=13-monthf+1
  allocate(goodfits(N_month,N_lat),dls(N_month,N_lat))
  goodfits='No'
  dls=dls_default

 print*,''
 print*,'finish reading all data,'
 call get_time(time00)
!!!!!!!!!!!1!!!!!!!!!!Allocate appropriate station arrays for correlation!!!!!!!!!

!  do reqmon=monthf,13  ! loop for monthly/annual
  do reqmon=monthf,monthf  ! loop for 1 monthly/annual, for Imke's test ONLY
    print*,monthnames(reqmon-1)

    do n=1,N_lat       ! loop for lat bands
      latF=-90.+sum(lat_width(1:n))-lat_width(n)
      latT=-90.+sum(lat_width(1:n))
      if(n==1) latF=latF-0.01   ! to include the South Pole
      N_sta=count((lat > latF) .and. (lat <= latT))
        bin_avgs=mdi
        goodfit='No'
        ctb=0
        print('(a,i2,a,i4)'),'lat_band:',n,',  N_sta=',N_sta
        call get_time(time00)
        nearest_cdd=dls_default   ! This line was added here due to problem found by Imke - H.Yang @ 2012.6.12
        if(N_sta >= 2) then    ! check station numbers within the lat band
          allocate(Ista(0:N_sta-1))
          i0=0
          do ii=0,nstats
            if((lat(ii) > latF) .and. (lat(ii) <= latT)) then
              Ista(i0)=ii
              i0=i0+1
            endif
          enddo
	 
          do j=0,N_sta-2  ! loop for stations
            N_mdiidx1=count((newdata(Ista(j),0,0:maxny-1) >= Syear) .and. (newdata(Ista(j),reqmon,0:maxny-1) /= mdi))
            if (N_mdiidx1 >= min_year) then
              allocate(mdiidx1(0:N_mdiidx1-1))
   !           arr1=mdi
              i0=0
              do ii=0,maxny-1
                if((newdata(Ista(j),0,ii) >= Syear) .and.(newdata(Ista(j),reqmon,ii) /= mdi)) then
                  mdiidx1(i0)=ii
                  i0=i0+1
                endif
              enddo
	     
!              N_mdiidx1=count(arr1 .ne. mdi)  !ascertain where missing data are

!only choose station pairs where at least min_year non-missing data are available in both arrays.

        
 !               avgarr1=sum(arr1(mdiidx1))/N_mdiidx1 !get the average for array1    ! What's its use?

                do k=j+1,N_sta-1    ! loop for neighbours
                  N_mdiidx2=count((newdata(Ista(k),0,0:maxny-1) >= Syear) .and. (newdata(Ista(k),reqmon,0:maxny-1) /= mdi))
                  if (N_mdiidx2 >= min_year) then
                    allocate(mdiidx2(0:N_mdiidx2-1))
!                    arr2=mdi
                    ii=0
                    do i0=0,maxny-1
                      if((newdata(Ista(k),0,i0) >= Syear) .and.(newdata(Ista(k),reqmon,i0) /= mdi)) then
                        mdiidx2(ii)=i0
                        ii=ii+1
                      endif
                    enddo

!                    N_mdiidx2=count(arr2 .ne. mdi) !ascertain where missing data are
		   
!only choose station pairs where at least 1 non-missing data are available in both arrays.

 		    
 !                     avgarr2=sum(arr2(mdiidx2))/N_mdiidx2 !get the average for array2

 !!!!!!!!!!!!!!!!!!!determine the years with available data in both arrays!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!only choose station pairs with at least min_year corresponding years!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!calculate correlation coefficient and significance!!!!!!!!!!!!!!!!!!!
 
                      n_yridx=0
                      N_yridx0=max(N_mdiidx1,N_mdiidx2)
                      allocate(yridx(0:N_yridx0-1))
                      call setIntersection(N_mdiidx1,N_mdiidx2,N_yridx0,mdiidx1,mdiidx2,yridx,n_yridx)
		      
                      if (n_yridx >= min_year) then
!                        rcorr=correlate(n_yridx,n_yridx,arr1(yridx(0:n_yridx-1)),arr2(yridx(0:n_yridx-1)))
                        rcorr=correlate(n_yridx,n_yridx,newdata(Ista(j),reqmon,yridx(0:n_yridx-1)),newdata(Ista(k),reqmon,yridx(0:n_yridx-1)))
                        var=1./(n_yridx-3.0)
                        if(rcorr==-1. .or. rcorr==1.) then
                          pvalue=0.
                        elseif(rcorr==mdi) then
                          pvalue=mdi
                        else
                          zvalue = 0.5*alog((1.+rcorr)/(1.-rcorr))/sqrt(var)
                          if (zvalue .lt. 0.) then
                            pvalue = 2.*gauss_pdf(zvalue)
                          else
                            pvalue = 2.*(1.-gauss_pdf(zvalue))
                          endif
                        endif
		      
!!!!!!!!!!!!!!!!!!!!!!determine bearings for each station pairing - required for ADW calculation!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		      
                        call Map_2points_para_meters(lon(Ista(j)),lat(Ista(j)),lon(Ista(k)),lat(Ista(k)),latlondist,parametersi)
                        call Map_2points_para_meters(lon(Ista(k)),lat(Ista(k)),lon(Ista(j)),lat(Ista(j)),tmp,parametersk)
                        thetak=acos(parametersk)
                        thetai=acos(parametersi)
                        binnum=int((latlondist*0.001)/100.)+1    !the first index is reserved for later so index will start from 1 (2??)
		      
                        if(binnum<=distance) then
                          ctb(binnum)=ctb(binnum)+1
                          bin_avgs(binnum,ctb(binnum))=rcorr
                        endif
		      
                      endif  !n_yridx .ge. min_year
		    
                      deallocate(yridx,mdiidx2)
		    
                    endif   !if (N_mdiidx2 .ge. min_year)
		   
                enddo  !k loop
	
                deallocate(mdiidx1)
	      
              endif   !if (N_mdiidx1 .ge. min_year)
	    
          enddo  !j loop for all nstats

          print('(a)'),'finish jj loop..'
          call get_time(time00)
!!!!!!!!!!!!!!!!!!!!!determine polynomial fit to data and goodness of fit!!!!!!!!!!!!!!!!!!!!!

          newavg(0)=1.
          do ii=1,distance
            N_newbinarray=count(bin_avgs(ii,0:999999) .ne. mdi)
            allocate(newbinarray(0:N_newbinarray-1))
            i0=0
            do i=0,999999
              if(bin_avgs(ii,i) .ne. mdi) then
                newbinarray(i0)=bin_avgs(ii,i)
                i0=i0+1
              endif
            enddo
            call moment(N_newbinarray,newbinarray,newavg(ii),sdevx(ii))
            deallocate(newbinarray)
          enddo

          epi=1./exp(1.)
          call poly_fit(distance+1,xbin,newavg,Ndegree,chisq,sigma,yfit,polyres)
	
          if (chisq .lt. error) then
            goodfit='No'
          else 
            goodfit='Yes'
          endif

          xfn=polyres(0)+polyres(1)*xn+polyres(2)*xn**2
          nc=count(xfn .lt. epi)

          if (nc .gt. 0) then
            do i0=0,distance*100-1    !find first index ...
              if(xfn(i0) .lt. epi) then
                nc=i0
                exit
              endif
            enddo
            nearest_cdd=xn(nc)
          else
            nearest_cdd=dls_default !if dls is less than search radius set to default
          endif

          dls(reqmon-monthf+1,n)=max(dls_default,nearest_cdd)
          goodfits(reqmon-monthf+1,n)=goodfit
          deallocate(Ista)

        else   ! if(N_sta >= 2)    ! check station numbers within the lat band

          dls(reqmon-monthf+1,n)=max(nearest_cdd,dls_default)
          goodfits(reqmon-monthf+1,n)=goodfit
          dls(reqmon-monthf+1,n)=dls_default
          goodfits(reqmon-monthf+1,n)='No'

        endif   !check if enough stations in the latitude band

        print('(a,i2)'),'finish lat_band:',n
        call get_time(time00)
        
      enddo  !loop for n latitude bands

    enddo   !loop for reqmon months

    print*,''
    print('(a,i3,a)'),'finish calculating the dls values on ',N_lat,' lat bands...'
    print*,''

    if(N_lat==1) then   ! check if only global dls was calculated.
      print*,'Only global stations were used in the calculation,'
      print*, 'so we will NOT continue for interpolation ...'
      print*,''
      print*,'And the dls values are:'
      print('(3x,<N_month>(a4,2x))'), (monthnames(i-1),i=monthf,13)
      print('(2x,<N_month>(i5,1x))'), (dls(i,1),i=1,N_month)
      stop
    endif

 print*,''
 print*,'finish calculating banded dls,'
 call get_time(time00)

    deallocate(lon,lat)

! read in gridded points (lat,lon)
    call getID(ID3)
    open(ID3,file=grid_file)
    cnt=0
    
    do
      read(ID3,*,end=200) r1
      cnt=cnt+1
    enddo
  
200 rewind(ID3)
    allocate(lata(0:cnt-1),lona(0:cnt-1),tempa(0:cnt-1),tmpa(cnt),tmpb(cnt))

    do i=0,cnt-1
      read(ID3,*) r1,r2
      lona(i)=r1
      lata(i)=r2
    enddo
 
    close(ID3)
 
    if(minval(lona)<0. .or. maxval(lata)>90.) then  ! exchange the lat and lon if necessary
      print*, 'NOTE: in "statfile", 1st column is latitude, 2nd is longitude !'
      tmpa=lata
      lata=lona
      lona=tmpa
    endif

    print('(a,i5,a)'),'finish reading all ',cnt,' locations (lon, lat) ...'

!!!!!!!!!!!!!!!!!!!!!!!extract all lats and lons from the grid file!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    tempa=lata  ! get all lat values
    i=1
 
    do
      tmpa(i)=minval(tempa)
      where(tempa == tmpa(i)) tempa=999.
      if(minval(tempa) == 999.) exit
      i=i+1
    enddo
 
    Nlat=i
    tempa=lona  ! get all lon values
    i=1
 
    do
      tmpb(i)=minval(tempa)
      where(tempa == tmpb(i)) tempa=999.
      if(minval(tempa) == 999.) exit
      i=i+1
    enddo
 
    Nlon=i
    print('(a,i3,a,i3,a)'),'There are ',Nlon,'/',Nlat,' lon/lat dimensions ...'

 print*,''
 print*,'finish reading all lat/lon data,'
 call get_time(time00)

    allocate(lon(Nlon),lat(Nlat))
    lon=tmpb(1:Nlon)
    lat=tmpa(1:Nlat)
    allocate(interp(Nlat),dlss(Nlon,Nlat,N_month))

!!!!!!!!!!!!!!!!!!!!!!interpolate dls values across lat bands (from grid file)!!!!!!!!!!!!!!!!!!!!!!

    do i=1,N_month
      call interpol(N_lat,dls(i,1:N_lat)*1.,latc,Nlat,lat(1:Nlat),interp)
      do j=1,Nlat
        dlss(1:Nlon,j,i)=max(interp(j),dls_default)
      enddo
    enddo

 print*,''
 print*,'finish calculating interpolation,'
 call get_time(time00)
!!!!!!!!!!!!!!!!!!!!!!!!!!!print some results to screen!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  print*,''
  print*,'Decorrelation length scales for each latitude and month:'
  print('(10x,<N_month>(a5,2x))'), (monthnames(i-1),i=monthf,13)
  
  do i=1,min(5,Nlat)
    print('(f7.3,2x,<N_month>(i6,1x))'), lat(i),(dlss(1,i,j),j=1,N_month)
  enddo

!!!!!!!!!!!!!!!!!!!!!!!!!extract grid file name and save it in the output file for gridding project!!!!!!!!!!!!!

  do j0=1,3
    if((grid_file(j0:j0)=='.') .and. (grid_file(j0+1:j0+1)/='.')) exit
  enddo
  
  grid_file(1:)=grid_file(j0+1:)
  i=len_trim(grid_file)-4  !the name should be *.txt

  do i0=i,1,-1
    if((grid_file(i0:i0)=='/') .or. grid_file(i0:i0)=='\') exit
  enddo
  
   i0=i0+1
   if(i0/=i) grid_file=grid_file(i0:i)

 print*,''
 print*,'finish finding grid_file,'
 call get_time(time00)

! output the dls values and grid file name in txt and/or nc files.
   call output

 print*,'finish outputing results, stop soon...'
 call get_time(time00)

  stop
end program distcorrfun_angles_wfun

! -----------------------------------------------------------------------------------------
! END MAIN PROGRAM - START SUBROUTINES
! -----------------------------------------------------------------------------------------

! -----------------------------------------------------------------------------------------
! The following subroutines are necessary for the code, specially designed for the project.
! Modification is needed when they're used for other projects.
! -----------------------------------------------------------------------------------------

!!!!!!!!!!!!!!!!!!!!!!!Linearly interpolate vectors with irregular grid!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!from IDL code, modified by H.Yang!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine interpol(m,V, X, n,xout, results)
 
 implicit none
 integer :: m,  &      ! # of input elements for x and V
            n,  &      ! # of elements for xout and results
            n0, &      ! # of valid elements in x and V
            i,j, &     ! loop counter
            indx(1),i1 ! index of array
 real :: nan                           !not a number - threshold for valid numbers
 real,dimension(m) :: x, &             !input monotonical independent array
                      V                !input ordinate of x
 real,dimension(n) :: xout,  &         ! The abscissa values for the result
                      results          ! result after interpolation
 real,allocatable, dimension(:) :: V1,x1 ! temporary arrays holding the valid input x and V.

  nan=1.e30        ! values outside this number is not valid...

  if(count(abs(xout) >= nan) >0) stop 'Error in Interpol: Xout has invalid values !'

  do i=2,m-1
    if((x(i)-x(i-1))*(x(i+1)-x(i)) <= 0.) stop 'Error in Interpol: the x must be monotonically ascending or descending !'
  enddo

  results=nan

!!!!!!!!!!!!!!!!!!!!!!!Filter out NaN values in both the V and X arguments!!!!!!!!!!!!!!!!!!

  n0=count((abs(v) < nan) .and. (abs(x) < nan) )  !This is valid data number.
  
  if(n0 <2) stop 'Error in Interpol: NOT enough valid numbers for input x and v !'
  allocate(x1(n0),v1(n0))
  
!!!!!!!!!!!!!!!!!!!!!!Make a copy so we don't overwrite the input arguments!!!!!!!!!!!!!!!!

  j=0
  
  do i=1,m
    if((abs(V(i)) < nan) .and. (abs(x(i)) < nan)) then
      j=j+1
      x1(j)=x(i)
      v1(j)=v(i)
    endif
  enddo
 
  do i=1,n
    indx=minloc(abs(x1-xout(i)))
    i1=indx(1)
    if((xout(i)-x(i1))*(x(i1+1)-xout(i)) < 0.) i1=i1-1
    i1=max(1,min(i1,n0-1))   ! XOUT[i] is between x[i1] and x[i1+1].
    results(i)=(xout(i)-x1(i1))/(x1(i1+1)-x1(i1))*(v1(i1+1)-v1(i1))+v1(i1)
  enddo

  deallocate(v1,x1)

  return
end subroutine interpol



!!!!!!!!!!!!!!!!!!Return parameters (such as distance, azimuth) relating to!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!the great circle or rhumb line connecting two points on a sphere.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!from IDL code, modified by H.Yang @ CCRC!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine Map_2points_para_meters(lon0, lat0, lon1, lat1,dis_meter,para) 

 implicit none
 double precision,parameter :: dpi=3.14159265358979323846264338d0   ! the ratio of the circumference to the diameter of a circle
 real :: lon0, lat0, lon1, lat1, & ! input locations (lon,lat) in degrees
         dis_meter,para            ! output distance (meters) and Cosine azimuth of the two locations
 double precision :: k,  &         ! constants to convert degrees into radian
                     r_earth, &    ! Earth radius (meters)
                     cosc,sinc,  & ! Cos and sin of angle between two locations
                     coslt1,coslt0,sinlt1,sinlt0,cosl0l1,sinl0l1, & ! temporary variables
                     cosaz,sinaz   ! cos and sin of azimuth

  if(abs(lat0) .gt. 90.) stop 'lat0 out of range [-90.,90] !'
  if(abs(lat1) .gt. 90.) stop 'lat1 out of range [-90.,90] !'

  k = dpi/180.d0
  r_earth = 6378206.4d0
  coslt1 = cos(k*lat1)
  sinlt1 = sin(k*lat1)
  coslt0 = cos(k*lat0)
  sinlt0 = sin(k*lat0)
  cosl0l1 = cos(k*(lon1-lon0))
  sinl0l1 = sin(k*(lon1-lon0))
  cosc = sinlt0 * sinlt1 + coslt0 * coslt1 * cosl0l1 !Cos of angle between two points
  
!!!!!!!!!!!!!!!!!!!!!!!!!Avoid roundoff problems by clamping cosine range to [-1,1]!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  if(cosc> 1.d0) cosc=1.d0
  if(cosc<-1.d0) cosc=-1.d0
  sinc = dsqrt(1.d0 - cosc**2)

  if (abs(sinc) .gt. 1.0d-7) then ! Small angle?
    cosaz = (coslt0 * sinlt1 - sinlt0*coslt1*cosl0l1) / sinc   ! Azimuth
    sinaz = sinl0l1*coslt1/sinc
  else		!Its antipodal
    cosaz = 1.d0
    sinaz = 0.d0
  endif

  dis_meter=acos(cosc) * r_earth
  para=cosaz  ! only return the first value ...

  return
end subroutine Map_2points_para_meters



!!!!!!!!!!!!!!!!This function computes the mean and Standard deviation of a given array x!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!from IDL code, modified by H.Yang @ CCRC!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine Moment(N, X, mean, Sdev)
 implicit none
 integer :: N                   ! element number of the input array
 real :: x(N), &                ! input array
         mean,Sdev, &           ! mean and Standard deviation of x
         var                    ! temporary variable
 real,allocatable :: resid(:)   ! residual array

!  if(n<2) stop 'Error in Moment: not enough input array'
  allocate(resid(n))
  Mean = sum(X) / n
  Resid = X - Mean

! Numerically-stable "two-pass" formula, which offers less
! round-off error. Page 613, Numerical Recipes in C.

  if(n>=2) then
    Var = (sum(Resid**2) - (sum(Resid)**2)/n)/(n-1.0)
  else
    Var=0.
  endif

  Sdev = sqrt(Var)  ! Standard deviation
  deallocate(resid)

  return
end subroutine Moment



!!!!!!!!!!!!!!!!!This function computes the linear Pearson correlation coefficient of two vectors!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!from IDL code, modified by H.Yang @ CCRC!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

real function correlate(Nx,Ny,X, Y)   !Vector inputs only.
use COMM !, only mdi  ! The "use .. only .." might not work for some compilers

 implicit none
 integer :: Nx,Ny,sLength                    ! number of array elements
 real :: x(Nx),y(Ny),  &                     ! input arrays
         xmean,ymean,dx2,dy2                 ! temporary variables
 real,allocatable :: tmp(:),xDev(:),yDev(:)  ! temporary arrays holding x y, and deviations

    !Means.
  sLength = min(Nx, Ny)
  allocate(tmp(sLength),xDev(sLength),yDev(sLength))
    
  tmp = x(1:sLength)
  xMean = sum(tmp) / sLength
  xDev = tmp - xMean
  tmp = y(1:sLength)
  yMean = sum(tmp) / sLength
  yDev = tmp - yMean
  dx2 = sum(abs(xDev)**2)
  dy2 = sum(abs(yDev)**2)
    
  if (dx2 .eq. 0. .or. dy2 .eq. 0.) then 
    correlate=mdi
  else
    correlate = sum(xDev * yDev)/ (sqrt(dx2)*sqrt(dy2))
    correlate = max(-1.,correlate)
    correlate = min(1., correlate)
  endif

  deallocate(tmp,xDev,yDev)
  return
  
end function correlate


!!!!!!!!!!!!!!!!!!!!Perform a least-square polynomial fit with optional error estimates!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!This routine uses matrix inversion!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! INPUTS:
!   X:  The independent variable vector.
!   Y:  The dependent variable vector, should be same length as x.
!   Ndegree: The degree of the polynomial to fit.
!
! OUTPUTS:
!   results is a vector of coefficients with a length of Ndegree+1.
!
!   CHISQ:   Sum of squared errors.
!
!   SIGMA:   The 1-sigma error estimates of the returned parameters.
!
!   YFIT:   Vector of calculated Y's. These values have an error
!           of + or - YBAND.

! from IDL code, modified by H.Yang @ CCRC.

subroutine poly_fit(n,x, y, ndegree, chisq, sigma, yfit,results)
 implicit none
 integer :: n,ndegree,status,i0,i1,m, & ! # of elements in coeff vec
            p,j,i,k                 ! loop integer counter
 real :: x(0:n-1),y(0:n-1),  &      ! input independent arrays
         results(0:ndegree), &      ! coefficients array
         yfit(0:ndegree-1),  &      !Vector of (polynomial fit) calculated Y's (least squares)
         sigma(0:ndegree),   &      !The 1-sigma error estimates of the returned parameters
         chisq                      ! Sum of squared errors
 double precision,allocatable :: covar(:,:), b(:), wy(:),z(:) ! temporary arrays
 double precision :: sum0,var       ! temporary variable

  m=ndegree + 1
  allocate(covar(0:m-1,0:m-1),b(0:m-1),wy(0:n-1),z(0:n-1))

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!construct work arrays!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!covar: least square matrix, weighted matrix!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!b:  will contain sum weights*y*x^j!!!!!!!!!!!!!!!!!!!!!!

  z = 1.d0          ! polynomial term (guarantees double precision calc)
  wy = 1.d0*y
  covar(0,0) =  n
  b(0) = sum(wy)

  do p = 1,2*ndegree   ! power loop
    z =z* x  ! z is now x^p
    if (p .LT. m) b(p) = sum(wy*z)   ! b is sum weights*y*x^j
    sum0 =  sum(z)
    do j = max(0,p-ndegree), min(ndegree, p)
      covar(j,p-j) = sum0
    enddo
  enddo ! end of p loop, construction of covar and b


  call inv_rg(m,covar, status)
  if(status/=1) pause 'Error in subroutine Inv_rg ...'
  results = matmul(b, covar)  ! construct coefficients

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!initialise one-standard deviation error estimates!!!!!!!!!!!!!!!

  yfit = results(ndegree)
   
  do k = ndegree-1, 0, -1
    yfit = results(k) + yfit*x  ! sum basis vectors
  enddo

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Evaluate vector of parameter errors!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  do i=0,M-1
    i0=i*(M+1)
    i1=mod(i0,(m-1))
    i0=(i0-i1)/(m-1)
    sigma(i) = sqrt(abs(covar(i0,i1)))
  enddo
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!No weighting, don't need to divide by sdev2!!!!!!!!!!!!!!!!!!!!!!

  chisq = sum((yfit - y)**2)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Unbiased experimental variance estimate!!!!!!!!!!!!!!!!!!!!!!!!!!!

  if(n > m) then
    var = chisq/(n-m) 
  else
    var= 0.
  endif

!!!!!!!!!!!!!!!Assume that the model is correct. In this case, SIGMA is multiplied by SQRT(chisq/(n-m))!!!!!!!
!!!!!!!!!!!!!!!See section 15.2 of Numerical Recipes in C (Second Edition) for details!!!!!!!!!!!!!!!!!!!!!!!!

  sigma = sigma*sqrt(chisq/(n-m))

  deallocate(covar, b, wy,z)

  return
end subroutine poly_fit



!!!!!!!!!!!!!!!This subroutine calculates the inverse of a given matrix A(N,N), double precision!!!!!!!!!!!!!
!
! input:
!    N : integer, dimension length of A
!    a : double precision matrix
!
! output:
!   a :  inversed matrix
!   error:  1 -- the calculation is successful
!           0 -- there's problem with the inverse
!
subroutine inv_rg(N,a,error)
 integer::N,   &    ! input array dimension length
          i,j,k, &  ! loop integer counters
          error     ! status of the calculation, 1 -- it's OK, 0 -- there's problem
 integer,allocatable::is(:),js(:)  ! arrays holding the index of a
 double precision::a(N,N),  &      ! input/output matrix
                   t,d             ! temporary variables

 allocate(IS(N),JS(N))
 error=1
  
 do k=1,n
    d=0.d0
    do i=k,n
      do j=k,n
	if (dabs(a(i,j)).gt.d) then
	  d=dabs(a(i,j))
	  is(k)=i
	  js(k)=j
	end if
      enddo
    enddo

   if (d+1.0.eq.1.0) then
      error=0
      print*,'err in inv_rg: not inverse !'
      return
   end if
	
   do j=1,n
     t=a(k,j)
     a(k,j)=a(is(k),j)
     a(is(k),j)=t
   enddo

   do i=1,n
     t=a(i,k)
     a(i,k)=a(i,js(k))
     a(i,js(k))=t
   enddo

   a(k,k)=1/a(k,k)
   do j=1,n
     if (j.ne.k) then
        a(k,j)=a(k,j)*a(k,k)
     end if
   enddo

   do i=1,n
     if (i.ne.k) then
       do j=1,n
         if (j.ne.k) then
           a(i,j)=a(i,j)-a(i,k)*a(k,j)
         end if
       enddo
     end if
   enddo  ! loop for i
   do i=1,n
     if (i.ne.k) then
       a(i,k)=-a(i,k)*a(k,k)
     end if
   enddo
 enddo   ! loop for k=1,n

 do k=n,1,-1
   do j=1,n
     t=a(k,j)
     a(k,j)=a(js(k),j)
     a(js(k),j)=t
   enddo
   do i=1,n
     t=a(i,k)
     a(i,k)=a(i,is(k))
     a(i,is(k))=t
   enddo
 enddo   ! loop for k=n,1,-1

 deallocate(is,js)

 return
end subroutine inv_rg



!!!!!!!!!!!!!!!!This function is used to find the intersection between two sets of integers.!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!RETURN A vector of values results(N_results) that are found in both set_a and set_b.!!!!!!!!!!!
!!!!!!!!!!!!!!!!maybe it's not very fast ...!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!from IDL code, modified by H.Yang @ CCRC!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

subroutine SetIntersection(Na,Nb,Nr,set_a, set_b, results, N_results)
 implicit none
 integer :: Na,Nb,Nr,  &                     ! dimension length of input arrays
            set_a(Na),set_b(Nb),&            ! input integer arrays
            results(Nr), &                   ! output integer array
            N_results                        ! # of valid elements in results
 integer :: mina,maxa,minb,maxb,minab,maxab,&  ! temporary variables
            i                                  ! integer loop counter

  if(Nr<Na .or. Nr<Nb) stop 'Error in SetIntersection -- dimension length wrong'
   
!Find the intersection of the ranges.

  mina = minval(set_a)
  maxa = maxval(set_a) 
  minb = minval(set_b)
  maxb = maxval(set_b)
  minab = max(mina, minb)
  maxab = min(maxa, maxb)
  
  if(maxa < minb .or. maxb < mina) then   ! If the set ranges don't intersect, leave now.
    N_results=0
    return
  endif

  N_results=0
  do i=minab,maxab      ! Find the intersection.
    if(count(set_a==i)>0 .and. count(set_b==i)>0) then
      N_results=N_results+1
      results(N_results)=i
    endif
  enddo

  return
end subroutine SetIntersection


!!!!!!!!!!!!!!!!The function evaluates the integral of the Gaussian probability function!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!! single precision only!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

real function gauss_pdf(a)

real,parameter :: err=2.e-7    ! error threshold for integral
real  :: a                     ! input parameter
real  :: x, x0,x1,x2,dxh,error ! temperary variable

 gauss_pdf=0.
 x0=-8.
 x=-abs(a)
 if(a>0. .and. x<=x0) gauss_pdf=1.-gauss_pdf
 if(x<=x0) return

 x2=x
 do
    dxh=(x2-x0)/2.;  x1=(x0+x2)/2.
    error=(func(x0)+func(x2)) - func(x1)*2.
    if(abs(error)  < err ) then
      gauss_pdf=gauss_pdf+(func(x0)+func(x2)+2.*func(x1))*dxh/2.
      x0=x2; x2=x
      if(x0 >= x) goto 100
    else
      x2=x1
    endif
 enddo

100 continue
 if(a>0.) gauss_pdf=1.-gauss_pdf

 end function gauss_pdf



  ! standard Gaussian function
real function func(x)
 double precision,parameter :: dpi=3.14159265358979323846264338d0  ! the ratio of the circumference to the diameter of a circle
 real :: x            ! input variable

  func=exp(-x**2./2.)/sqrt(2.*dpi)
end function func



! ******* to get a free unit ID for I/O file.
! input
!    ID # 
! output
!   ID # that is not in use now

subroutine getID(ID)
 integer :: ID,  &   ! unit ID for I/O file
            ios      ! status indicator
 logical :: lopen    ! whether a file (unit ID) is opened

  ID=10
  do 
    inquire(unit=ID,opened=lopen,iostat=ios)
    if(ios==0.and.(.not.lopen)) return
    ID=ID+1
  enddo

  return
end subroutine getID


 subroutine get_time(current)
  integer      :: tarr(8),ii
  character*23 :: file
  character*45 :: printf
  double precision :: current

  call Date_and_Time(values=tarr)
  write(file,12) tarr(1),tarr(2),tarr(3),tarr(5),tarr(6),tarr(7),tarr(8)
12  format(i4.4,'.',i2.2,'.',i2.2,' ',i2.2,':',i2.2,':',i2.2,'.',i3.3)

  printf='current time: '//file

  current= tarr(5)*3600.d0+tarr(6)*60.d0+tarr(7)+0.001d0*tarr(8)

  write(*,'(a,/)') printf

  return
  end subroutine get_time 
