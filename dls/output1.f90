!...   This program is to create an NetCDF file.
!...   And the dimention is three dimentions (lon, lat, time)
 subroutine output
 use COMM
 implicit none
 
  print*,''
  print*,'finish calculating, now output results in ascii and netcdf files ...'
  call out_txt
  call out_nc(1)
  call out_nc(2)
  return
 
 end subroutine output
 



  subroutine out_nc(n)
    use COMM
    use netcdf
    implicit none

    character*350 :: outfile
    character*20 :: ctit(2),cname(2)
    integer :: ncid, LATID, LONID, TIMEID,i,n                ! dimension's ID
    integer varID_lon,varID_lat,varID_time,ID_f(13)
    integer dataDim(3)
    integer start(3), count(3)
    data start /1,1,1/
    data ctit/'Calc grid box values','number of stations'/
    data cname/'gridval','numbers'/

    if(n/=1 .and. n/=2) stop 'Error in out_nc: the input parameter n is out of range [1,2] !'
!   print*,lon(1),lon(Nlon),lat(1),lat(Nlat),time(1),time(maxny)
    count=(/Nlon,Nlat,maxny/)
    if(n==1) outfile=trim(out_file)//'.nc'
    if(n==2) outfile=trim(out_file)//'_num.nc'

!... begin to creat a new NetCDF file
    if(NF90_create(Outfile, NF90_clobber, ncid) /=NF90_noerr) then
      print*,'Error: can NOT create the following output file:'
      print*,trim(Outfile)
      stop 'Now the code stops ...'
    endif

!... begin to define your dimentions
    call err_handle(NF90_def_dim(ncid, 'lon', Nlon, LONID),'define "lon" dimension')
    call err_handle(NF90_def_dim(ncid, 'lat', Nlat, LATID),'define "lat" dimension')
    call err_handle(NF90_def_dim(ncid, 'time', maxny, TIMEID),'define "time" dimension')

    call err_handle(NF90_def_var(ncid, 'lon', NF90_float, lonID,varID_lon),'define var "lon"')
    call err_handle(NF90_def_var(ncid, 'lat', NF90_float, latID,varID_lat),'define var "lat"')
    call err_handle(NF90_def_var(ncid, 'time', NF90_int, timeID,varID_time),'define var "time"')

!... begin to define varibles
    datadim=(/lonid,latid,timeid/)

    do i=1,Nmonth
      call err_handle(NF90_def_var(ncid, trim(monthnames(mon_cal(1)+i-2)), NF90_float, datadim, ID_f(i)),'define data var')
      call err_handle(NF90_put_att(ncid, ID_f(i), 'missing_value',mdi),'put att for data var')
      call err_handle(NF90_put_att(ncid, ID_f(i), '_FillValue', mdi),'put att for data var')
    enddo

    call err_handle(NF90_put_att(ncid, NF90_global, 'Title', trim(cname(n))),'define global title')
    call err_handle(NF90_put_att(ncid, NF90_global, 'author',  'Hongang Yang - hongang.yang@unsw.edu.au ') ,'define global author')
    call err_handle(NF90_put_att(ncid, NF90_global, 'history', 'Created from Gridding version 2.5.2 '),'define global history')
!   call err_handle(NF90_put_att(ncid, NF90_global, 'units',  trim(units(nindx))),'define global unit')
    call err_handle(NF90_put_att(ncid, NF90_global, 'long_name', trim(ctit(n))),'define global long_name')

    call err_handle(NF90_put_att(ncid, varID_lon, 'long_name', 'Longitude'),'put att for "lon"')
    call err_handle(NF90_put_att(ncid, varID_lon, 'units', 'degrees_east'),'put att for "lon"')
    call err_handle(NF90_put_att(ncid, varID_lon, 'axis', 'X'),'put att for "lon"')

    call err_handle(NF90_put_att(ncid, varID_lat, 'long_name', 'Latitude'),'put att for "lat"')
    call err_handle(NF90_put_att(ncid, varID_lat, 'units',  'degrees_north'),'put att for "lat"')
    call err_handle(NF90_put_att(ncid, varID_lat, 'axis', 'Y'),'put att for "lat"')

    call err_handle(NF90_put_att(ncid, varID_time, 'units', 'day as %Y%m%d.%f'),'put att for "time"')
    call err_handle(NF90_put_att(ncid, varID_time, 'calendar', 'proleptic_gregorian'),'put att for "time"')

    call err_handle(NF90_enddef(ncid),'end define')

    call err_handle(NF90_put_var(ncid, varID_lon, lon),'save "lon" var')
    call err_handle(NF90_put_var(ncid, varID_lat, lat),'save "lat" var')
    call err_handle(NF90_put_var(ncid, varID_time, time),'save "time" var')

   if(n==1) then
     do i=1,Nmonth
       call err_handle(NF90_put_var(ncid, ID_f(i), gridval(:,:,:,i)),'save data gridval')
     enddo
   else
     do i=1,Nmonth
       call err_handle(NF90_put_var(ncid, ID_f(i), numbers(:,:,:,i)),'save data numbers')
     enddo
   endif
   call err_handle(NF90_close(ncid),'close nc file')

  return
  end subroutine out_nc



subroutine err_handle(status,string)
 use netcdf
 implicit none
 integer       :: status
 character*(*) :: string

 if(status/=NF90_noerr) then
   print*,'Error in Netcdf function: '//trim(string)
   print*,'Error code: ',status
   print*,'Please check the "out_nc" subroutine.'
   stop 'now code stops ...'
 endif

 return
 end subroutine err_handle