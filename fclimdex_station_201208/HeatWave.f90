! -------------------------------------------------------------------------
! To calculate Excess Heat Indices proposed by Nairn, Fawcett and Ray,
! with modification by Lisa Alexander, and Sarah Perkins.
! Document from Lisa and Sarah.
! 
! definitions are:
! ADT: Average Daily Temperature, 
!        ADT=(Tmax + Tmin)/2
! 
! EHIs: Excess Heat,   significance index 
!        EHI_sig=(T_i+T_{i-1}+T_{i-2})/3.-T_95
!        T_95 is the 95th percentile of daily temperature (Ti) for the climate BASE period
!
! EHI: Heat Stress,  acclimatisation index
!     EHI_accl=(T_i+T_{i-1}+T_{i-2})/3.-(T_{i-3}+...+T_{i-32})/30
! 
! EHF: Excess Heat Factor,  EHF=EHI_sig*max(1,EHI_accl)             ! Note the change here !!!
! 
! There are three set of the above indices: for x (TMAX), n (TMIN) and a (ADT).
! 
! HWA/HWD/HWF/HWM/HWN are calculated annually, for warm spells AND summer only events
! HWA: Heat Wave Amplitude, the hottest day (or peak) of an event with the highest average magnitude in a given year
! HWD: Heat Wave Duration, the average length of all heat waves per year
! HWF: Heat Wave Frequency, the total number of days within a year that meet the heat wave criteria.
! HWM: Heat Wave Mean, the average daily magnitude across all heat wave events within the year
! HWN: Heat Wave Number, the total number of heat waves in a year
! 
! input
!    none
!   (transfer data through the module COMM)
! note that thres90p(1:DOY,1:4)=[thresan90(1:DOY),thresax90(1:DOY), 0.*(1:DoY), 0.*(1:DoY)]

! list of Ofile(46-66):
!   'TN90p_HWA','TN90p_HWD','TN90p_HWF','TN90p_TWM','TN90p_HWN', &
!   'TX90p_HWA','TX90p_HWD','TX90p_HWF','TX90p_TWM','TX90p_HWN', &
!   'EHF_HWA','EHF_HWD','EHF_HWF','EHF_TWM','EHF_HWN', &
!   'EHF_sp_HWA','EHF_sp_HWD','EHF_sp_HWF','EHF_sp_TWM','EHF_sp_HWN', &
!    'EHI_sp'/


     subroutine Heatwave
      use COMM
      use functions 
      implicit none

      integer                      :: i,k,ID, m,n,yr,mo,dy, nn
      real,dimension(MaxYear*DoY)  :: ADT, EHIx,EHIn,EHIa, EHIsx,EHIsn,EHIsa,EHFx,EHFn,EHFa,EHIs_sp, EHF_sp
      real,dimension(YRS*DoY)      :: TX,TN,TA, TA_sp,T95   ! remove the YYYY0229
      real,dimension(BYRS,DoY+2*SS):: tdata  ! extended T_ave data in base period
      real,dimension(BYRS,DoY)     :: tmp    ! base year T_ave, removing YYYY0229
      real,dimension(DoY)          :: ttmp  ! 95th percentile based on the calendar day (BASE period), removing YYYY0229
      real,dimension(YRS,3)        :: YMD0  ! date excluding YYYY0229
      
      if(Tmax_miss .or. Tmin_miss) return

      ! initialise the indices
      ADT  =MISSING
      EHIx =MISSING
      EHIn =MISSING
      EHIa =MISSING
      EHIsx=MISSING
      EHIsn=MISSING
      EHIsa=MISSING
      EHFx =MISSING
      EHFn =MISSING
      EHFa =MISSING
      EHI_sp=MISSING

! get ADT, Average Daily Temperature
      ADT=(TMAX+TMIN)/2.
      where(TMAX==MISSING) ADT=MISSING
      where(TMIN==MISSING) ADT=MISSING


! get all EHI indices
     call get_EHI(TOT,TMAX(1:TOT),EHIx(1:TOT),EHIsx(1:TOT),EHFx(1:TOT))  ! max
     call get_EHI(TOT,TMIN(1:TOT),EHIn(1:TOT),EHIsn(1:TOT),EHFn(1:TOT))  ! min
     call get_EHI(TOT, ADT(1:TOT),EHIa(1:TOT),EHIsa(1:TOT),EHFa(1:TOT))  ! ave

! remove YYYY0229 from the original data
     m=0
     n=0
     do yr=1,YRS
       year=yr+SYEAR-1
       nn=0
       do mo=1,12
         do dy=1,MON(mo,1)
           m=m+1
           n=n+1
           TX(m)=TMAX(n)
           TN(m)=TMIN(n)
           TA(m)=EHFa(n)
           YMD0(m,:)=YMD(n,:)
           if(year.ge.BASESYEAR.and.year.le.BASEEYEAR) then
             nn=nn+1
             tmp(yr+SmByear,nn)=ADT(n)
           endif
         enddo
         if(leapyear(year)==1 .and. mo==2) n=n+1  ! jump YYYY0229
       enddo
     enddo
     print*,m,n
     
     thres90p(:,3:4)=0.   ! threshold for EHF related HWI
     
    ! get T95, which is 90th percentile based on the calendar day (BASE period). 
 ! extended TA in base years
     tdata(1,1:SS)=tmp(1,1)  ! i=1
     tdata(2:BYRS,1:SS)=tmp(1:BYRS-1,DoY+1-SS:DoY) ! i /=1
     tdata(:,1+SS:DoY+SS)=tmp(:,1:DoY)
     tdata(BYRS,1+DoY+SS:DoY+SS*2)=tmp(BYRS,DoY)
     tdata(1:BYRS-1,1+DoY+SS:DoY+SS*2)=tmp(2:BYRS,1:SS)
     k=0
     call threshold(tdata,0.90,1,ttmp,k)  ! thresholds for T_ave
     ttmp(:)=ttmp(:)+1.e-5
     if(k==1) then
       write(6,*) "T_ave Missing value overflow in exceedance rate"
       ttmp=MISSING
     endif
     k=1
     m=1
     do i=1,YRS
       n=Leapyear(i+SYEAR-1)  ! leap year? (1 = yes, 0 = no)
       T95(k:k+DoY-1)=ttmp(1:DoY)
       TA_sp(k:k+58)=ADT(m:m+58)             ! T_ave, removing YYYY0229
       TA_sp(k+59:k+DoY-1)=ADT(m+59+n:m+DoY+n-1)
       k=k+DoY
       m=m+DoY+n
     enddo
      
! get EHIs_sp, significance index with new T95.
     i=1
     do
        call getday(TOT,TA_sp(1:TOT),3,i,k)
        if(k==0) goto 200
        EHIs_sp(k)=sum(TA_sp(k-2:k))/3.-T95(k)
        do
          k=k+1
          i=i+1
          if(k>n) goto 200
          if(nomiss(TA_sp(k))) then
            EHIs_sp(k)=sum(TA_sp(k-2:k))/3.-T95(k)
          else
            i=k+1
            goto 201
          endif
        enddo
201     continue
      enddo
200   continue   ! finish searching all data

! get EHF, which is the Excess Heat Factor
      EHF_sp=max(1.,EHIa)*EHIs_sp         ! note the change here ! H.Yang @ 20120831
      where(EHIa   ==MISSING) EHF_sp=MISSING
      where(EHIs_sp==MISSING) EHF_sp=MISSING
           
     !           data  #  I/O ID
     call get_HW(TN,    1,46)
     call get_HW(TX,    2,51)
     call get_HW(TA,    3,56)
     call get_HW(EHF_sp,4,61)  ! EHF_sp_HW*
     
     deallocate(thres90p)
     
! output the results.
! Note that we didn't remove first/last successive all-MISSING data yet here.
     k=40
     call getID(ID)
     open(ID,file=ofile(k))
     write(ID,'(a)') 'Year Month  Day    EHIx    EHIn    EHIa   EHIsx   EHIsn   EHIsa    EHFx    EHFn    EHFa'
     do i=1,TOT
        write(ID,'(3(i4,1x),9f8.1)') YMD(i,1),YMD(i,2),YMD(i,3),EHIx(i),EHIn(i),EHIa(i), &
                                EHIsx(i),EHIsn(i),EHIsa(i),EHFx(i),EHFn(i),EHFa(i)
     enddo
     close(ID)
     
     k=66
     call getID(ID)
     open(ID,file=ofile(k))
     write(ID,'(a)') 'Year Month  Day    EHIa    EHI_sp   EHF_sp  ! note there"s no YYYY0229'
     do i=1,YRS*DoY        ! We removed YYYY0229
        write(ID,'(3(i4,1x),3f8.1)') YMD0(i,1),YMD0(i,2),YMD0(i,3),EHIa(i),EHIs_sp(i),EHF_sp(i)
     enddo
     close(ID)
     
     
    
     return
     end subroutine HeatWave


! -------------------------------------------------------------------------
!  get each EHI, EHIs, EHF index
!
   subroutine get_EHI(n,data,EHIa,EHIs,EHF)
   use COMM
   use functions
   implicit none
   integer           :: n,i,j,k,indx(1),cnt
   real,dimension(n) :: data,EHIa,EHIs,EHF
   real,allocatable  :: data0(:)
   real              :: T95

  ! get EHIa, acclimatisation Excess Heat Index
      i=1
      do
        call getday(n,data,32,i,k)
        if(k==0) goto 100   ! end of searching?
        EHIa(k)=sum(data(k-2:k))/3.-sum(data(k-32:k-3))/30.
        do
          k=k+1
          i=i+1
          if(k>n) goto 100      ! end of searching?
          if(nomiss(data(k))) then       ! next data is also good?
            EHIa(k)=sum(data(k-2:k))/3.-sum(data(k-32:k-3))/30.
          else                           ! no, so search again !
            i=k+1
            goto 101
          endif
        enddo
101     continue
      enddo
100   continue   ! finish searching all data

! get T95 which is 95th percentile of daily temperature during the BASESYEAR and BASEEYEAR..
     indx=minloc(abs(YMD(:,1)-BASESYEAR))
     k=indx(1)
     indx=minloc(abs(YMD(:,1)-BASEEYEAR-1))
     j=indx(1)-1
     cnt=j+1-k
     allocate(data0(cnt))
     data0(1:cnt)=data(k:j)    ! get data during the base period
     call percentile(data0, cnt, 1, 0.95, T95)
     deallocate(data0)

! get EHIs, significance index
      i=1
      do
        call getday(n,data,3,i,k)
        if(k==0) goto 200
        EHIs(k)=sum(data(k-2:k))/3.-T95
        do
          k=k+1
          i=i+1
          if(k>n) goto 200
          if(nomiss(data(k))) then
            EHIs(k)=sum(data(k-2:k))/3.-T95
          else
            i=k+1
            goto 201
          endif
        enddo
201     continue
      enddo
200   continue   ! finish searching all data

! get EHF, which is the Excess Heat Factor
      EHF=max(1.,EHIa)*EHIs         ! note the change here ! H.Yang @ 20120831
      where(EHIa==MISSING) EHF=MISSING
      where(EHIs==MISSING) EHF=MISSING
      
     return
     end subroutine get_EHI


! -------------------------------------------------------------------
! This subroutine calculates the LAST index of FIRST m consecutive valid data,
!     starting from Ifrom.
!  input:
!      data: input data array
!      n : element # of array data
!      m: required consecutive day #
!     Ifrom: first index to start the searching with
!
!  output:
!     Iresult: the required index (if successful)
!              0 (end of searching, no good result)

    RECURSIVE subroutine getday(n,data,m,Ifrom,Iresult)
    use COMM    !,only MISSING
    use functions
    integer :: n,m,Ifrom,Iresult,i,j
    real    :: data(n)

    Iresult=0
    if(m+Ifrom-1 > n) return   ! end of searching, no good result

    i=Ifrom

    if(count(data(i:(i+m-1)) /= MISSING) ==m ) then  ! find good array and get LAST index
      Iresult=i+m-1
      return
    endif

    do j=i+m-1,i,-1      ! no good array, so continue searching, get last good index
      if(ismiss(data(j))) exit
    enddo
    call getday(n,data,m,j+1,Iresult)   ! continue searching

    return
    end subroutine getday
    
    
    
! to calculate Sarah's Heat Wave Index.
!
! input:
!    T : temperature or EHF, length is YRS*DoY, removing data on YYYY0229.
!    n : indicator of T: 1 --> TN, 2--> TX, 3--> EHF, 4--> EHF_sp.
!   n0: ID # for output of 5 indices: n0, n0+1, n0+2, n0+3, n0+4
!  (thres90p was transfered through COMM)
!
! output:
!   five monthly and annual indices: HWA, HWD, HWF, HWM, HWN, are saved in separate files
!
   subroutine get_HW(T,n,n0)
   use COMM
   use functions
   implicit none
   integer                  :: n,n0,i,j,k,ii,m,m0.m1,yr, ID
   real, dimension(YRS*DoY) :: T
   real, dimension(YRS,13)  :: HWA, HWD, HWF, HWM, HWN
   real, dimension(DoY)     :: temp,peak,ave
   integer,dimension(DoY)   :: spell
   integer,dimension(1)     :: idx
   real,dimension(31)       :: tt
   
   HWA=MISSING  ! initialise the indices
   HWD=MISSING
   HWF=0
   HWM=MISSING
   HWN=0
   
   k=1
   do yr=1,YRS
     temp=T(k:k+DoY-1)
     spell=0
     peak=MISSING
     ave =MISSING
     ii=1      ! find spells, and the avg and peak of temp during the period
     do i=1,DoY
       if(temp(i)> thres90p(i,n)) then
         spell(ii)=spell(ii)+1
       else
         if(spell(ii)>3) then
           peak(ii)=maxval(temp(ii:ii+spell(ii)-1))
           ave(ii) =sum(temp(ii:ii+spell(ii)-1))/spell(ii)
         endif
         ii=i+1
       endif
     enddo
     
     m0=1
     do m=1,12   ! get monthly index
       m1=m0+MON(m,1)-1
       j=count(spell(m0:m1)>0)
       if(j>=1) then
          HWN(yr,m)=j                   ! number of heatwaves per month.
          idx=maxloc(ave(m0:m1))
          HWA(yr,m)=peak(m0+idx(1)-1)   ! peak of HOTTEST heatwave per month.
          HWF(yr,m)=sum(spell(m0:m1))   ! freuqnecy of days in heatwave per month.
          HWD(yr,m)=maxval(spell(m0:m1)) ! duration of longest heatwave per month.
          tt=MISSING;  tt(1:MON(m,1))=ave(m0:m1);  where(tt <= MISSING+1.) tt=0.
          HWM(yr,m)=sum(tt)/j           ! taking the monthly avg acoss the avg magnitude per heat wave.
       endif
       m0=m1+1
     enddo

     m=13           ! get annual index
     j=count(spell>0)
     if(j>=1) then
        HWN(yr,m)=j              ! number of heatwaves per month.
        idx=maxloc(ave)
        HWA(yr,m)=peak(idx(1))   ! peak of HOTTEST heatwave per month.
        HWF(yr,m)=sum(spell)     ! freuqnecy of days in heatwave per month.
        HWD(yr,m)=maxval(spell)  ! duration of longest heatwave per month.
        where(ave<=MISSING+1.) ave=0.    ! note that AVE was corrupted
        HWM(yr,m)=sum(ave)/j     ! taking the monthly avg acoss the avg magnitude per heat wave.
     endif
     
     k=k+DoY
   
   enddo    ! year loop
   
   ! output the 5 indices in separate files, removing consecutive all-MISSING data at the very beginning and end of the years!
   
   call save_monthly(n0  ,HWA)
   call save_monthly(n0+1,HWD)
   call save_monthly(n0+2,HWF)
   call save_monthly(n0+3,HWM)
   call save_monthly(n0+4,HWN)    
   
   return
   end subroutine get_HW
   