! We need to read data from netcdf files ...
! 
! different versions of NetCDF has different function name and augments, too confusing...
!
! question:
! 1. the type of lon,lat,time is 6 (double)
!  the type of tmax is 5 (real)
!  However, I read lon,lat,tmax as real, time as integer. Why?
!
subroutine inp_nc_f90
 use COMM
 use functions
 use netcdf
 implicit none
 integer,dimension(3) :: ncid,ID_time,ID_lon,ID_lat,ID_var,len1,len2,len3,len
 integer :: i,j,k,i1,i2,i3,it,ky,month,kth,ii,jj,kk,now
 integer :: ndim(3),indx(1)
 real :: miss
 character*(120)  :: file(3),title,cname,attribute
 real,allocatable :: value2(:,:)
 integer,dimension(:),allocatable  :: t1,t2,t3 
! real,dimension(:),allocatable  :: t1,t2,t3 
 real,allocatable,dimension(:,:,:) :: prc,tmx,tmn
 character*10,dimension(3) :: dim_name  !,cvar
 logical :: exists
! include 'netcdf.inc'
! data cvar/'tmax','tmin','pre'/      ! They must be in correct order
 data dim_name/'lon','lat','time'/   ! They must be in correct order

  file(1)=trim(tmax_dir)//trim(cTmax)
  file(2)=trim(tmin_dir)//trim(cTmin)
  file(3)=trim(prcp_dir)//trim(cPrcp)

!inquire(file=file(2),iostat=status,exist=lexist)
! print*,status,lexist
!print*,'netCDF version ',NF90_INQ_LIBVERS()

  do i=1,3
    i1=len_trim(file(i))
    if(file(i)(i1-2:i1) .ne. '.nc')   file(i)=trim(file(i))//'.nc'
    inquire(file=file(i),Exist=exists)
    if(.Not.exists) then
        print*,'the following input file doesn"t exist, code stops:'
        print*,trim(file(i))
        stop
    endif
    call err_handle(nf90_open(file(i),NF90_nowrite,NCID(i)), 'open file')
    call err_handle(NF90_INQUIRE(NCID(i),i1,i2),'inqure dim/var numbers')
    if(i1 /=3) stop 'Error: wrong dimension numbers !'
!    call err_handle(NF90_INQ_NVARS(NCID(i),i1),'inq var number')
    if(i2 /=4) stop 'Error: wrong variable numbers !'
  enddo

! get three coordinates (dimensions), they should be the same for all three data....
  do i=1,3
    call err_handle(NF90_INQ_DIMID (NCID(i), dim_name(1), ID_lon(i)),'inq ID for lon')
    call err_handle(NF90_INQ_DIMID (NCID(i), dim_name(2), ID_lat(i)),'inq ID for lat')
    call err_handle(NF90_INQ_DIMID (NCID(i), dim_name(3), ID_time(i)),'inq ID for time')
    call err_handle(NF90_INQUIRE_DIMENSION (NCID(i),ID_lon(i), len=len1(i)),'inq dim_len for lon')
  enddo
  if(len1(1)/=len1(2).or.len1(1)/=len1(3)) stop 'Error: different dimension size for lon ...'
  ndim(1)=len1(1)
  do i=1,3
    call err_handle(NF90_INQUIRE_DIMENSION (NCID(i),ID_lat(i), len=len1(i)),'inq dim_len for lat')
  enddo
  if(len1(1)/=len1(2).or.len1(1)/=len1(3)) stop 'Error: different dimension size for lat ...'
  ndim(2)=len1(1)
  do i=1,3
    call err_handle(NF90_INQUIRE_DIMENSION (NCID(i),ID_time(i), len=len1(i)),'inq dim_len for time')
  enddo
!     if(len1(1)/=len1(2)) stop 'Error: Tmin and Tmax have different size in time ...'   ! maybe this is not necessary
  allocate(t1(len1(1)),t2(len1(2)),t3(len1(3)))
  allocate(tmx(ndim(1),ndim(2),len1(1)),tmn(ndim(1),ndim(2),len1(2)),prc(ndim(1),ndim(2),len1(3)))

! get the two dimension variables - lon and lat
  do i=1,2
       allocate(value2(ndim(i),3))
       do j=1,3
         call err_handle(NF90_INQ_VARID (NCID(j), dim_name(i), i1),'inq var ID')
         call err_handle(NF90_get_var(ncid(j),i1,value2(:,j)),'get Var real')
       enddo
        if(value2(1,1)/=value2(1,2).or.value2(1,1)/=value2(1,3)) stop 'Error: different lon/lat range ...'
        if(value2(ndim(i),1)/=value2(ndim(i),2).or.value2(ndim(i),1)/=value2(ndim(i),3)) stop 'Error: different lon/lat range ...'
       if(i==1) then
          allocate(lon(ndim(i)))
          lon=value2(:,1)    ! get x coordinate 
       else
          allocate(lat(ndim(i)))
          lat=value2(:,2)    ! get y coordinate 
       endif
       deallocate(value2)
  enddo

! get dimension variable - time
     do i=1,3
         call err_handle(NF90_INQ_VARID (NCID(i), dim_name(3), len3(i)),'inq var ID for dimensions')
     enddo
       attribute=''
     call err_handle(NF90_get_var(ncid(1),len3(1),t1),'get Var int -- t1')
     If(NF90_GET_ATT(ncid(1),len3(1),'units',attribute) == NF90_noerr) call time_convert(len1(1),t1,attribute)

       attribute=''
     call err_handle(NF90_get_var(ncid(2),len3(2),t2),'get Var int -- t2')
     If(NF90_GET_ATT(ncid(2),len3(2),'units',attribute) == NF90_noerr) call time_convert(len1(2),t2,attribute)

       attribute=''
     call err_handle(NF90_get_var(ncid(3),len3(3),t3),'get Var int -- t3')
     If(NF90_GET_ATT(ncid(3),len3(3),'units',attribute) == NF90_noerr) call time_convert(len1(3),t3,attribute)
!   print*,'t1:',t1(1:3),t1(len3(1))
!   print*,'t2:',t2(1:3),t2(len3(2))
!   print*,'t3:',t3(1:3),t3(len3(3))

! get data Var ...
     do i=1,3
        do i1=1,4
          call err_handle(NF90_INQUIRE_VARIABLE(NCID(i), i1,ndims=i2),'inq VarNdims')
          if(i2==3) len3(i)=i1   ! find data Var ID
        enddo
!       call err_handle(NF90_INQ_VARID (NCID(i), cvar(i), len3(i)),'inq var ID for data')
       call err_handle(NF90_INQUIRE_VARIABLE (NCID(i), len3(i),dimIDs=len2),'inq Var dim ID')
       if(len2(1)/=ID_lon(i) .or. len2(2)/=ID_lat(i) .or. len2(3)/=ID_time(i)) stop "The dim ID for the data var is not correct: should be in order of (lon,lat,time) !"
     enddo
     call err_handle(NF90_get_var(ncid(1),len3(1),tmx),'get Var real -- Tmax')
     if(NF90_GET_ATT(NCID(1), len3(1),'missing_value',miss) == NF90_noerr) then
        where(tmx == miss) tmx=MISSING
     endif
     if(NF90_GET_ATT(NCID(1), len3(1),'_FillValue',miss) == NF90_noerr) then
        where(tmx == miss) tmx=MISSING
     endif
     call err_handle(NF90_get_var(ncid(2),len3(2),tmn),'get Var real -- Tmin')
     if(NF90_GET_ATT(NCID(2), len3(2),'missing_value',miss) == NF90_noerr) then
        where(tmn == miss) tmn=MISSING
     endif
     if(NF90_GET_ATT(NCID(2), len3(2),'_FillValue',miss) == NF90_noerr) then
        where(tmn == miss) tmn=MISSING
     endif
     call err_handle(NF90_get_var(ncid(3),len3(3),prc),'get Var real -- PRCP')
     if(NF90_GET_ATT(NCID(3), len3(3),'missing_value',miss) == NF90_noerr) then
        where(prc == miss) prc=MISSING
     endif
     if(NF90_GET_ATT(NCID(3), len3(3),'_FillValue',miss) == NF90_noerr) then
        where(prc == miss) prc=MISSING
     endif

     do i=1,3
       call err_handle(NF90_CLOSE(NCID(i)),'close file')
     enddo

    Syear=min(t1(1),t2(1),t3(1))/10000
    Eyear=max(t1(len1(1)),t2(len1(2)),t3(len1(3)))/10000
    YRS=Eyear-Syear+1
!    print*,YRS
    allocate(time(YRS))
    tot=0
    do i=Syear,Eyear
      tot=tot+365
      if(leapyear(i)==1) tot=tot+1
      time(i+1-Syear)=i*10000.+101.
    enddo
    print*,'Total data #:',tot

     allocate(YMD(tot,3),data_tmax(ndim(1),ndim(2),tot),data_tmin(ndim(1),ndim(2),tot),data_prcp(ndim(1),ndim(2),tot))

     it=0
      do i=SYEAR,EYEAR
        Ky=leapyear(i)+1
        do month=1,12
          Kth=Mon(month,Ky)
          do k=1,kth
            it=it+1
!      if(abs(it-790)<3) print*,it,ky,kth,k
            YMD(it,1)=i
            YMD(it,2)=month
            YMD(it,3)=k
!        if(i==1992 .and. month==9) print*,k,it,tmx(1,1,it),tmn(1,1,it),prc(1,1,it)
          enddo
        enddo
      enddo
     if(it/=tot) stop 'Error: different TOT and IT in inp_nc !'
!     print*,len1,tot
!stop

      jj=1 ! This was part of the original QC subroutine -- get full data and insert MISSING values..
      ii=1
      kk=1
      do i=1, tot
        now=YMD(i,1)*10000+YMD(i,2)*100+YMD(i,3)        ! H.Yang
!      if(abs(i-790)<3) print*,i,ii,jj,kk,now,t1(ii),t2(jj),t3(kk)
!      if(i /= ii) then
!         print*,i,ii,jj,kk
!         stop
!      endif

!      if(mod(i,100) == 0) print*,'mod:',i,ii,jj,kk
!      if(i>986) stop

!       if(i>975 .and. i<985) then 
!         print*,i,ii,jj,kk,now
!         print*,t1(ii),t2(jj),t3(kk)
!       endif

        if(ii>len1(1)) then
!          print*,ii,len1(1)
          data_tmax(:,:,i)=MISSING
        else
          if(now.eq.t1(ii)) then
             data_tmax(:,:,i)=tmx(:,:,ii)
             ii=ii+1
          else
             data_tmax(:,:,i)=MISSING
          endif
        endif

        if(jj>len1(2)) then
          data_tmin(:,:,i)=MISSING
        else
          if(now.eq.t2(jj)) then
             data_tmin(:,:,i)=tmn(:,:,jj)
             jj=jj+1
          else
             data_tmin(:,:,i)=MISSING
          endif
        endif

        if(kk>len1(3)) then
          data_prcp(:,:,i)=MISSING
        else
          if(now.eq.t3(kk)) then
             data_prcp(:,:,i)=prc(:,:,kk)
             kk=kk+1
          else
             data_prcp(:,:,i)=MISSING
          endif
        endif

 !     if(YMD(i,1)==1992 .and. YMD(i,2) ==9 .and. YMD(i,3)==1) then
 !     endif

      enddo

!         print*,data_tmax(1,1,975:985)
!         print*,data_tmin(1,1,975:985)
!         print*,data_prcp(1,1,975:985)


   Nlon=ndim(1);  Nlat=ndim(2);  Ntime=tot !ndim(3)
   deallocate(t1,t2,t3,prc,tmx,tmn)

  print*,'finish reading Tmax,Tmin and PRCP data ...'
 ! stop

return
end subroutine inp_nc_f90