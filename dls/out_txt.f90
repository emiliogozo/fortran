
 subroutine out_txt
   use COMM
   implicit none
   integer       :: ID0,status,i,j,k,n,IDcpu,OMP_GET_THREAD_NUM
   character*350 :: outfile
   
!   outfile=trim(out_file)//'.txt'
!   open(ID0,file=outfile,IOstat=status)
!   if(status/=0) then
!     print*, 'Error: can NOT create the following file to save results:'
!     print*,trim(outfile)
!     stop 'Now code stops !'
!   endif

 IDcpu=0

!$OMP parallel default(shared) private(n,i,j,k,ID0,status,outfile,IDcpu); IDcpu=OMP_GET_THREAD_NUM()
!$OMP do
 do n=1,Nmonth
   outfile=trim(out_file)//'_'//monthnames(n-2+Mon_cal(1))//'.txt'
   ID0=10+IDcpu
   call getID(ID0)
   open(ID0,file=outfile,IOstat=status)
   if(status/=0) then
     print*, 'Error: can NOT create the following file to save results:'
     print*,trim(outfile)
     stop 'Now code stops !'
   endif
   do j=0,Nlat-1
     do i=0,Nlon-1
       do k=0,maxny-1
         write(ID0,'(2F9.4,1x,I4,F7.1,I4)') lon(i+1),lat(j+1),simvalyr(k), gridval(i,j,k,n),int(numbers(i,j,k,n))
       enddo
     enddo
   enddo
   close(ID0)
 enddo
!$OMP end do
!$OMP end parallel

 return
 end subroutine out_txt